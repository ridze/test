Ext.define('MyApp.view.logIn.LogIn', {
    extend: 'Ext.form.Panel',
    xtype: 'logIn',

    reference:'logIn',
    
    controller: 'logIn',
    model: 'logIn',
   
  
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    header:{
    width:'100%',
    padding:0,   
    items:[
    {
        xtype:'container',
        width:'100%',
        height:65,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        margin:0,
        items:[{
                xtype:'button',
                text:'Login',
                cls: 'register-header-shadow',
                flex:1,
                height:65
        },{
                xtype:'button',
                text:'Register',
                cls: 'login-header-shadow',
                flex:1,
                height:65,
                handler:'registerBtn'                
        }]
    }]
    },
    items: [{
        xtype: 'textfield',
        label: 'Username',
        name:'username',
        reference:'fieldOne',
        listeners: {
            change:'onFormClick'
        },
        required: true
    }, {
        xtype: 'passwordfield',
        label: 'Password',
        name:'password',
        reference:'fieldTwo',
        listeners: {
            change:'onFormClick'
        },
        required: true
    }],
    html:'<br>',
    buttonAlign : 'center', 
    buttons:[{
        text: 'submit',
        handler:'logIn',
        width:'50%',
    }] 
    
        
});