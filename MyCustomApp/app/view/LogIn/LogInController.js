Ext.define('MyApp.view.logIn.LogInController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.logIn',

    getDemandedHash: function(){
        return localStorage.getItem('demandedHash');
    },

    registerBtn: function(){
        this.redirectTo('Register');
    },

    onFormClick: function(){
        this.getView().setHtml('<br>');
    },

    logIn: function () {

        if (this.getView().getValues()['username'] && this.getView().getValues()['password']) //Validacija
        {


            Ext.Ajax.request({
                defaultHeaders: {
                    'Content-Type': 'application/json'

                },
                scope: this,

                url: 'http://localhost:8080/users/login',
                params: Ext.encode(this.getView().getValues()),

                success: function (response, opts) {

                    localStorage.setItem('dbToken',Ext.JSON.decode(response.responseText).token);
                    localStorage.setItem('token', this.lookup('fieldOne').getValue());
                    this.redirectTo(this.getDemandedHash() ? this.getDemandedHash() : 'Dashboard');
                    localStorage.setItem('demandedHash', '');
                    this.getView().setHtml('<br>');


                },

                failure: function (response, opts) {
                    this.getView().setHtml('<span style="color:#ef6c00">Wrong username or password</span>');

                    console.log('server-side failure with status code ' + response.status);

                }
            });
        }
        else {
            Ext.Msg.alert('Please, fill all fields.');
        }
    }




});