Ext.define('MyApp.view.logIn.LogInModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.logIn',

    fields: [{
        name: 'username',
        validators: 'presence'
    }, {
        name: 'password',
        validators:  'presence'
    }]




});
