Ext.define('MyApp.view.addProject.AddProject', {
    extend: 'Ext.form.Panel',
    xtype: 'addProject',

    reference:'addProject',
    
    controller: 'addProject',
    model: 'addProject',
    
    layout:{
        type:'vbox',
        align:'stretch'
    },
    title:'Project Name',
    items:[{
        xtype:'textareafield',
        grow      : true,
        name      : 'description',
        label:'Project Description',
        anchor    : '100%'
    },{
        xtype: 'textareafield',
        label: 'Task 1'
    }],


    buttons:[{
        text:'+Add Task',
        handler:'addTask'
    }]
    

        
});