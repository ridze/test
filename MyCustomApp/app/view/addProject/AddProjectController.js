Ext.define('MyApp.view.addProject.AddProjectController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.addProject',

    addProject: function () {
        var i;
        var functions = this;
        var wrapper = this.lookupReference('buttonsWrapper');
        Ext.Msg.prompt(
            'Enter title for new project',
            '',
            function (buttonId, value) {
                if (value) {
                    Ext.Ajax.request({
                        defaultHeaders: {
                            'Content-Type': 'application/json'
                        },

                        scope: this,

                        url: 'resources/ulazni_podaci.json',

                        success: function (response, opts) {
                            var res = Ext.JSON.decode(response.responseText);
                            var ajaxData = res.projects;
                            var ind = 0;
                            for (i = 0; i < ajaxData.length; i++) {
                                if (value == ajaxData[i].name) {
                                    ind++;
                                }
                            }

                            if (ind > 0) {
                                Ext.Msg.alert('Title already exists');
                            }
                            else {
                                var myBtn = Ext.create({
                                    xtype: 'button',
                                    handler: function () {
                                        functions.toProject(myBtn.getText()); //id
                                    },
                                    text: value,
                                    style: 'height:80px;margin:0px 20px 20px 20px;background-color:green;border-radius:10px'
                                });
                                wrapper.add({xtype: 'container', items: [myBtn]});
                                // Ext.Ajax.request({   POST na server, ne radi sa json fajlom
                                //  defaultHeaders: {
                                //         "Content-type": "application/json"
                                //     },
                                //     url: 'resources/ulazni_podaci.json',
                                //     method: 'POST',
                                //     params: {'id':i, 'name': value},
                                //     success: function(conn, response, options, eOpts) {
                                //         console.log(i);
                                //     },
                                //     failure: function(conn, response, options, eOpts) {
                                //         console.log('server-side failure with status code ' + response.status);
                                //     }
                                // });
                            }
                        },

                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);

                        }
                    });
                }
            });
    },

    addTask: function () {
        var taskField = Ext.create({
            xtype: 'textareafield',
            label: 'Task '
        });
        this.getView().add(taskField);

    }


});