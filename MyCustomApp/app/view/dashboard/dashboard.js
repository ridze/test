Ext.define('MyApp.view.dashboard.dashboard', {
    extend: 'Ext.Container',
    xtype: 'dash',

    reference: 'dash',

    controller: 'dashboard',
    viewModel: 'dashboard',

    listeners:{
        activate:'loadData'
    },
    style:'height:calc(100vh - 90px)',
    autoScroll: true,
    scrollable: true,
    buttonAlign: 'right',
    items: [{
        xtype: 'container',
        hidden:true,
        layout: {type: 'hbox', align: 'middle'},
        items: [{
            xtype: 'spacer'
        }, {
            xtype: 'button',
            text: 'logout',
            handler: 'logOut',
            //style: 'color:black'
        }]

    }, {
        xtype: 'panel',
        cls: 'register-form',
        maxHeight: '100%',
        layout: {type: 'vbox', align: 'stretch'},


        title: 'Projects list',
        cls: 'panel-title-bg',
        items: [{
            xtype: 'container',
            items: [{

                xtype: 'textfield',
                label: 'Search',
                name: 'search',
                reference: 'dashSearch',
                style: 'padding-left:20px;padding-right:20px',
                listeners:{
                    change:'onSearchFieldChange'
                }

            }]
        }, {
            xtype: 'button',
            text: 'Add New Project',
            style: 'margin:10px 0px',
            handler: 'addProject'
        },
            {
                xtype: 'panel',

                flex: 1,
                scrollable: true,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                reference: 'buttonsWrapper',
                items: []

            }]

    }]
});
