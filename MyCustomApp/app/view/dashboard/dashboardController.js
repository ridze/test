Ext.define('MyApp.view.dashboard.dashboardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.dashboard',
    listen: {
        controller: {
            'project': {
                'getProj': function () {
                    this.AllProjects();
                }
            }
        }
    },

    toProject: function (id) {
        this.redirectTo('Project/' + id);

    },
    logOut: function () {
        localStorage.setItem("token", "");
        this.redirectTo("Login");

    },
    toAddProject: function () {
        this.redirectTo('AddNewProject');

    },
    AllProjects: function () {
    },
    loadData: function () {
        var wrapper = this.lookupReference('buttonsWrapper');
        this.lookup('dashSearch').clearValue();
        
        Ext.Ajax.request({
            defaultHeaders: {
                "Content-Type": "application/json",
                "Token" : localStorage.getItem('dbToken')
                
            },
            scope: this,
            url: 'http://localhost:8080/projects',
            method: "GET",
            success: function (response, options) {

                var ajaxData = Ext.JSON.decode(response.responseText);
                var functions = this;
                wrapper.setItems([]);
                
                this.AllProjects = function () {
                    return ajaxData;
                };
                var projectNames = [];
                ajaxData.forEach(function (item) {
                    projectNames[item.id] = item.name;
                    var button = Ext.create({
                        xtype: 'button',
                        handler: function () {
                            functions.toProject(item.id);
                        },
                        itemId: item.id,
                        text: item.name,
                        cls: 'project-btn'
                    });
                    wrapper.add(button);
                });
                localStorage.setItem("projectNames", JSON.stringify(projectNames));
            },
            failure: function (res, opt) {
                console.error('Errror occurred during ajax call. Status: ' + res.status);
            }
        })
    },

    addProject: function () {
        var i;
        var functions = this;
        var wrapper = this.lookupReference('buttonsWrapper');
        Ext.Msg.prompt(
            'Enter title for new project',
            '',
            function (buttonId, value) {
                if (value && buttonId=="ok") {
                    Ext.Ajax.request({
                        defaultHeaders: {
                            'Content-Type': 'application/json',
                            "Token" : localStorage.getItem('dbToken'),
                        },
                        url: 'http://localhost:8080/projects',
                        method: 'POST',
                        params: Ext.encode({name: value, fromDate: new Date(), toDate: new Date(), description:""}),
                        success: function (conn, response, options, eOpts) {
                            functions.loadData();
                        },
                        failure: function (conn, response, options, eOpts) {
                            console.log('server-side failure with status code ' + response.status);


                        }
                    });
                }
            });
    },

    onSearchFieldChange: function(component, newV){
        
        buttonsWrapper = this.lookup('buttonsWrapper');
        buttonsWrapper.items.each(function(item){
            
            var i=0, buttonNameLength = item.getText().length, searchedTextLength = newV.length;
            var notSearchedButton = item;
            while(i<buttonNameLength-searchedTextLength+1){
                
                if(item.getText().slice(i, i+searchedTextLength).toUpperCase() == newV.toUpperCase()){
                    var searchedButton = item; 
                    break;
                };    
                i++;
            };
            if (searchedButton) searchedButton.setHidden(false);
            else notSearchedButton.setHidden(true);

        });
    }


});
