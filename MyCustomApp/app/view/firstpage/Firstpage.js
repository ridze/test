Ext.define('MyApp.view.firstpage.Firstpage', {
    extend: 'Ext.Panel',
    xtype: 'firstpage',
    reference: 'firstpage',

    requires: [
        'Ext.MessageBox',
        'Ext.layout.Fit',
    ],
    
    controller: 'firstpage',
    viewModel: 'firstpage',
    
    listeners:{
        show: 'onFirstpageShow'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [{
        xtype:'container',
        //cls: 'home-bg',
        style:'width:100vw; height:100vh; background: url("slika.png") no-repeat fixed center; background-size: cover;',
        layout: {
            type: 'vbox',
            align: 'middle'
        },  
        buttonAlign:'center',
        items:[{
            xtype:'container',
            cls: 'home-text',
            //margin: '0 10px',
            reference:'startupTxt1',
            id:'txt1',
            layout:{type:'vbox',align:'middle'},
            padding:0,
            style:'margin-top:10vh',
            html:"<p>Greetings.</p>",
     
                
        },{
            xtype:'container',
            cls: 'home-text',
            //margin: '0 10px',
            reference:'startupTxt2',
            id:'txt2',
            layout:{type:'vbox',align:'middle'},
            padding:0,
            hidden:true,
            html:"<p>Welcome to Job Locks!</p>",

     
                
        },{
            xtype:'container',
            cls: 'home-text',
            //margin: '0 10px',
            reference:'startupTxt3',
            id:'txt3',
            layout:{type:'vbox',align:'middle'},
            padding:0,
            hidden:true,
            html:"<p>Mobile app that helps many firms get the very best of their employees!</p>",
     
        },{
            xtype:'container',
            cls: 'home-text',
            //margin: '0 10px',
            reference:'startupTxt4',
            id:'txt4',
            layout:{type:'vbox',align:'middle'},
            style:'margin-bottom:0px',
            padding:0,
            hidden:true,
            items:[{
                html:"<p>To find out more - tap the start button.</p>",

            },{
                xtype:'button',
                id:'txt42',
                iconCls:'md-icon-keyboard-arrow-down',
            }]
            
     
        },{
            xtype: 'button',
            text: 'Start',
            handler: 'start',
            reference:'startBtn',
            width:'40%',
            style:'border:1px solid;border-radius:25px;margin-top:5px',
            id:'txt21',
            hidden:true,

        }]
    }]

});
