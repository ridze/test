Ext.define('MyApp.view.firstpage.FirstpageController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.firstpage',

    start: function() {

        this.redirectTo('Login');

    },

    fadeIn:function(id){
	
        var component = Ext.get('txt' + id);
        component.animate({
            duration: 1200,
            from:{
            	opacity:0
            },
            to: {
                opacity: 1
            },          
        });
        
    },
    
    onFirstpageShow: function() {
        this.getView().suspendEvents();
    	var functions = this;    	
    	var i = 1;
    	

    	setTimeout(function(){functions.fadeIn(1);}, 0);
    	setTimeout(function(){
            functions.lookup('startupTxt2').setHidden(false);
            functions.fadeIn(2);
        }, 2000); 
        setTimeout(function(){
            functions.lookup('startupTxt3').setHidden(false);
            functions.fadeIn(3);
        }, 4200);   
        setTimeout(function(){
            functions.lookup('startupTxt4').setHidden(false);
            functions.fadeIn(4);
        }, 6000);
        setTimeout(function(){
            functions.lookup('startBtn').setHidden(false);
            functions.fadeIn(21);
            
        },6400);
     



    	
    }	





});