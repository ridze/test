Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.Container',
    xtype: 'app-main',

    requires: [

        'Ext.MessageBox',
        'Ext.layout.Fit',


    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        }
    },

    tabBarPosition: 'bottom',

    layout: {
        type: 'vbox',
    },
    items: [{
        xtype: 'firstpage',
        cls: 'fadein-onload-page',
        hidden: true,
        listeners:{
            activate:'hideNavButtons'
        }

    },{
        xtype: 'container',
        cls: 'dash-logo-bg',
        reference: 'gornji',
        height: 47,
        layout: {
            type: 'hbox',
        },
        items:[{
            xtype:'button',
            cls: 'logo',
            text:'looooggggoooo',
            handler:'goToDashboard'
        },{
            xtype:'spacer'
        },{
            xtype:'button',
            text:'logout',
            handler:'logOut',
            iconCls:'md-icon-exit-to-app',
            style: 'color:var(--background-color);font-weight:bold;text-shadow:0 0 1px var(--background-color);',
            iconAlign:'right'
        }]
    }, {
        xtype: 'register',
        cls: 'fadein-onload-page',
        hidden: true,
        listeners: {
            activate: 'hideNavButtons',
            show: 'hideIfLoggedIn'
        }

    }, {
        xtype: 'logIn',
        cls: 'fadein-onload-page',
        hidden: true,
        listeners: {
            activate: 'hideNavButtons',
            show: 'hideIfLoggedIn'
        }

    }, {
        xtype: 'dash', 
        cls: 'fadein-onload-page',
        hidden: true,
        itemId: 'dash',
        listeners: {
            show: 'showIfLoggedIn',
            activate: 'showNavButtons'
        }
    }, {
        xtype: 'project', 
        cls: 'fadein-onload-page',
        hidden: true,
        listeners: {
            show: 'showIfLoggedIn',
            activate: 'showNavButtons'
        }
    }, {
        xtype: 'addProject',
        cls: 'fadein-onload-page',
        hidden: true,
        listeners: {
            activate: 'showNavButtons'
        }

    }, {
        xtype: 'taskReview',
        cls: 'fadein-onload-page panel-title-bg',
        hidden: true,
        listeners: {
                show: 'showIfLoggedIn',
            activate: 'showNavButtons'
        }

    }, {
        xtype: 'spacer'
    }, {
        xtype: 'container',
        reference: 'navButtons',
        hidden: true,
        layout: {
            type: 'hbox',
        },
        items: [{
            xtype: 'button',
            text: 'previous',
            handler: 'goPreviousBtn',
            width: '45%',
            iconCls: 'md-icon-chevron-left'
        }, {
            xtype: 'spacer'
        }, {
            xtype: 'button',
            text: 'dash',
            handler: 'goToDashboard',
            width: '45%',
            iconCls: 'md-icon-home'

        }]
    }


    ],

});
