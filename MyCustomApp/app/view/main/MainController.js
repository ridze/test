Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    
    routes: {
        'Firstpage':'hideAllExceptTheFirstpage',
        'Login':'hideAllExceptTheLogin',
        'Register':'hideAllExceptTheRegister',
        'Dashboard':'hideAllExceptTheDashboard',
        'Project/:id': 'hideAllExceptTheProject',
        'Project/:pid/task/:tid': 'hideAllExceptTheTask',
        'AddNewProject': 'hideAllExceptTheAddNewProject'

    },
    hideAllExceptTheFirstpage:function(){
        this.hideAllExceptTheRoute('firstpage');
    },
    hideAllExceptTheLogin:function(){
        this.lookup('logIn').reset();
        this.hideAllExceptTheRoute('logIn');
    },
    hideAllExceptTheRegister:function(){
        this.lookup('register').reset();
        this.hideAllExceptTheRoute('register');
    },
    hideAllExceptTheDashboard:function(){
        this.hideAllExceptTheRoute('dash');
    },
    hideAllExceptTheProject:function(){
        this.hideAllExceptTheRoute('project');
    },
    hideAllExceptTheTask:function(){
        this.hideAllExceptTheRoute('taskReview');
    },
    hideAllExceptTheAddNewProject(){
        this.hideAllExceptTheRoute('addProject');
    },
    hideAllExceptTheRoute: function(xtype){
        this.getView().getActiveItem().hide();
        this.getView().setActiveItem(xtype);
        this.getView().lookup(xtype).show();
    },
       
    getToken: function(){
        scope:this;
        return localStorage.getItem('token');
    },

    showIfLoggedIn:function(component){
        if(!this.getToken()){
            component.setHidden(true);
            Ext.Msg.alert('Unauthorized access', 'Please login to proceed', 'loggedInRefreshRoute', this);   
            
        }
    },
    hideIfLoggedIn:function(component){
        if(this.getToken()){
            component.setHidden(true);
            this.redirectTo('Dashboard');   
            
        }

    },
    goPreviousBtn: function(){
        history.back();
    },

    goToDashboard: function(){
        this.redirectTo('Dashboard');
    },    

    loggedInRefreshRoute:function(){

        this.redirectTo(this.getToken() ? 
            //Logged In
            ((window.location.hash === '') ? 
                'Dashboard' : 
                window.location.hash) : 
            //Not Logged In
            ((window.location.hash === '#Register' || window.location.hash === '#Login' || window.location.hash === '#Firstpage') ?
                window.location.hash : 
                (window.location.hash === '') ?
                    'Firstpage' :
                    'Login'


            ));
        if(this.getToken()) localStorage.setItem('demandedHash', '');
    },
    
    showNavButtons: function(){
        this.lookup('navButtons').setHidden(false);
        this.lookup('gornji').setHidden(false);
    },

    hideNavButtons:function(){
        this.lookup('navButtons').setHidden(true);
        this.lookup('gornji').setHidden(true);
    },

    logOut: function () {
        functions = this;
        Ext.Msg.confirm('Logout','Are you sure you want to logout?', function(button){
             if(button == 'yes'){
                  localStorage.setItem("token", "");
                  functions.redirectTo("Login");
             }
        });

    },

    init:function(){
        localStorage.setItem('demandedHash', '');
        if (window.location.hash && window.location.hash != '#Register' && window.location.hash != '#Login' && window.location.hash != '#Firstpage')
            localStorage.setItem('demandedHash', window.location.hash);

        this.loggedInRefreshRoute();
    }
    

});
