Ext.define('MyApp.view.project.Project', {
    extend: 'Ext.form.Panel',
    xtype: 'project',
    reference: 'project',

    controller: 'project',
    viewModel: 'project',
    header: {
      titlePosition: 0,
      titleAlign: 'center',
      items: [{
              xtype: 'button',
              iconCls: 'md-icon-assignment',
              align: 'right',
              handler:'editProj'
            }],
    },
    scrollable: true,
    height: 'calc(100vh - 80px)',

        maxHeight: '100%',
    listeners: {
        show: 'loadInfo',
    },
    layout: {
        type: 'vbox',
        align: 'stretch',
    },
    items: [{
        reference: 'myItems',
        xtype: 'panel',

        overflowY: 'scroll',
        items: [],
        buttons: [{
            iconCls: 'md-icon-library-add',
            text: 'ADD TASK',
	          handler: 'newTask',
		//style: 'background-color:#2196f3;color:white; margin:10px',

        }]

    }]


});