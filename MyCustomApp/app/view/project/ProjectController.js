Ext.define('MyApp.view.project.ProjectController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.project',
    tasktap: function (idx) {
        console.log(`Task ${idx} tapped!`);
    },
    goBack: function () {
        window.history.back();
    },
    applyTasks: function () {
        console.log('Tasks Applied');
    },
    goToTask: function (taskid) {
        var projectId = window.location.hash.slice(9);
        this.redirectTo('Project/' + projectId + "/task/" + taskid);
    },
    projInfo: function () {
      return {};
    },
    deleteProj: function () {
      var storedNames = JSON.parse(localStorage.getItem("projectNames"));
      var currentProjectId = window.location.hash.slice(9);
      var currentProjectName = storedNames[currentProjectId];
      var par = this;
      Ext.Msg.confirm('Delete project','Are you sure you want to delete this project?', function (btnText) {
            if(btnText == 'yes'){
              Ext.Ajax.request({
                method: "DELETE",
                defaultHeaders: {
                  "Content-Type" : "application/json",
                  "Token" : localStorage.getItem('dbToken'),
                },
                url: `http://localhost:8080/projects/${currentProjectName}`,
                scope:this,
                success: function (res,opts) {
                  
                  Ext.Msg.alert("Delete","You have successfuly deleted this project!", function() {
                  window.history.back();
Ext.getCmp('ProjDialog').destroy();
                });
                },
                failure: function (res,opts) {
                  Ext.Msg.alert("Error","Server-side failure with status code " + res.status);
                  console.log("\n\n%c x %cYou missed the path?", 'font-size:0px;padding:50px 35px;line-height:100px;background:url("https://media.giphy.com/media/Jlfm6Z6pAy8bC/giphy.gif"),radial-gradient(closest-side, rgba(255,255,255,1), rgba(0,0,0,0));background-size:100% 100%; background-repeat:no-repeat;text-align:center;width:70px;margin:0px auto 0px;border-radius:50%;', 'font-family: Arabic;font-size:16px; line-height:100px;vertical-align:middle;color:black;');
                }
              });
            }
			});
    },
    editProj: function () {
      var me = this;
      var storedNames = JSON.parse(localStorage.getItem("projectNames"));
      var currentProjectId = window.location.hash.slice(9);
      var currentProjectName = storedNames[currentProjectId];
      var dialog = Ext.create({
         id:'ProjDialog',
         xtype: 'dialog',
         header: {
           titlePosition: 0,
           titleAlign: 'left',
           items: [{
                   xtype: 'button',
                   iconCls: 'md-icon-delete',
                   align: 'right',
                   handler:me.deleteProj,
                 }],
         },
         title: 'Project Edit',
         layout: {
           type: 'vbox',
         },
         maximizable: true,
         buttons: [{
           text: 'OK',
           handler: function () {
             Ext.Ajax.request({
                 defaultHeaders: {
                     'Content-Type': 'application/json',
                     'Token': localStorage.getItem('dbToken')
                 },
                 url: `http://localhost:8080/projects/${currentProjectName}`,
                 method: 'PUT',
                 params: Ext.encode({
                   name: Ext.getCmp('editProjName').getValue(),
                   fromDate:  String(me.projInfo().fromDate).slice(0,10),
                   toDate: Ext.Date.format(Ext.getCmp('editDatePicker').getValue(), ('Y-m-d')),
                   description: Ext.getCmp('editProjDesc').getValue()
                 }),
                 success: function (res) {
                     dialog.destroy();
		     me.loadInfo();
                 },
                 failure: function (res) {
                     Ext.Msg.alert("Error","Server-side failure with status code " + res.status);
                 }
             });
           }
         },{
           text: 'CANCEL',
           handler: function () {
             dialog.destroy();
           }
         }],
     });
     var storedNames = JSON.parse(localStorage.getItem("projectNames"));
     var currentProjectId = window.location.hash.slice(9);
     var currentProjectName = storedNames[currentProjectId];
      Ext.Ajax.request({
        method: 'GET',
        url: 'http://localhost:8080/projects/'+currentProjectName,
        defaultHeaders: {
          "Content-Type" : "application/json",
          "Token" : localStorage.getItem('dbToken'),
        },
        success: function (res,opts) {
          var resText = Ext.JSON.decode(res.responseText);
          var myItems =  [{
            xtype: 'textfield',
            id: 'editProjName',
            label: 'Title',
            value: resText.name,
          },{
            flex:1,
            xtype: 'textareafield',
            id: 'editProjDesc',
            label: 'Description',
            value: resText.description,
          },{
            margin: '10px 0px 10px',
            xtype: 'datepickerfield',
            id: 'editDatePicker',
            value: new Date(resText.toDate),
            label: 'End date',
          }];
          dialog.setItems(myItems);
          dialog.show();
        },
        failure: function (res,opts) {

                Ext.Msg.alert("Error","Server-side failure with status code " + res.status);
        }
      });


    },
    newTask: function() {
        var par = this;
        var projId = Number(window.location.hash.slice(9));
        Ext.Msg.prompt('Enter your task name','',function(buttonId,value){
          if(value && buttonId == 'ok'){
            var dateNow = Ext.Date.format(new Date(), ('Y-m-d'));
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:8080/tickets",
              defaultHeaders: {
                "Content-Type" : "application/json",
                "Token" : localStorage.getItem('dbToken'),
              },
              params: Ext.encode({
                  _usernameAssignee: null,
                  _usernameCreator: localStorage.getItem('token'),
                  description: "none",
                  points: 0,
                  project_id: window.location.hash.slice(9),
                  resolvedDate: String(dateNow).slice(0,10),
                  status: "TO_DO",
                  summary: value,
                  ticketType: "NEW_FEATURE",
                  updatedDate: String(dateNow).slice(0,10),
              }),
              success: function (conn, response, options, eOpts) {
                console.log("TASK CREATED");
                par.loadInfo();

              },
              failure: function (res) {
                  Ext.Msg.alert("Error","Server-side failure with status code " + response.status);
              }
            });
          }


        });
    },
    loadInfo: function () {
        var storedNames = JSON.parse(localStorage.getItem("projectNames"));
        var currentProjectId = window.location.hash.slice(9);
        var currentProjectName = storedNames[currentProjectId];
        this.getView().setTitle(currentProjectName.toUpperCase());

        var myUrl = "http://localhost:8080/projects/"+currentProjectName+"/tickets";
        console.log(myUrl);
        var me = this;
        Ext.Ajax.request({
            defaultHeaders: {
                "Content-Type": "application/json",
                "Token" : localStorage.getItem('dbToken')
            },
            scope: this,
            url: 'http://localhost:8080/projects/'+currentProjectName,
            method: "GET",
            success: function (responseProj, optionsProj) {
              var resProj = Ext.JSON.decode(responseProj.responseText);
              me.projInfo = function () {
                return resProj;
              };
              Ext.Ajax.request({
                  defaultHeaders: {
                      "Content-Type": "application/json",
                      "Token" : localStorage.getItem('dbToken')
                  },
                  scope: this,
                  url: myUrl,
                  method: "GET",
                  success: function (response, options) {
                      var res = Ext.JSON.decode(response.responseText);

                      console.log(res);
                      var getDate1 = function() {
                        var getDate = String(resProj.fromDate).substring(0,10);
                        if(getDate != 'null')
                          return getDate;
                        else return 'Not displayed';
                      };
                      var getDate2 = function() {
                        var getDate = String(resProj.toDate).substring(0,10);
                        if(getDate != 'null')
                          return getDate;
                        else return 'Not displayed';
                      };
                      var itemsArr = [{
                          xtype: 'panel',
                          items: [{
                            xtype:'panel',
                            minHeight:'20vh',
                            items:[{
                              xtype:'panel',
                              html:`<p style="text-align:justify;">
                                      <b>Description: </b> ${(resProj.description != null && resProj.description != "") ? resProj.description : 'No description avalaible.'}
                                    </p>`,
                            },{
                              layout: {
                                type: 'hbox'
                              },
                              items:[{
                                flex:1,
                                html: `<p style="float:left; margin:20px 0px 20px 0px;">
                                        <b>Start date: </b> ${getDate1()}
                                      </p>`
                              },{
                                flex:1,
                                html: `<p style="float:right; margin:20px 0px 20px 0px;">
                                        <b>End date: </b> ${(getDate1() == getDate2()) ? '<span style="color:lightgreen">Unlimited</span>' : getDate2()}
                                      </p>`
                              }]
                            }],
                          }]
                      }];


                      res.forEach(function (item, idx) {
                        function getStatus () {
                          if(item.status == 'TO_DO')return '<b style="color:lightgreen;">ACTIVE</b>';
                          else if(item.status == 'IN_PROGRESS')return '<b style="color:orange;">IN PROGRESS</b>';
                          else if(item.status == 'RESOLVED')return '<b style="color:blue;">RESOLVED</b>';
                          else return '<b style="color:red;">DISABLED</b>';
                        }
                        function getType () {
                          if(item.ticketType == 'NEW_FEATURE')return '<b style="color:lightgreen;">NEW FEATURE</b>';
                          else if(item.ticketType == 'IMPROVEMENT')return '<b style="color:lightgreen;">IMPROVEMENT</b>';
                          else if(item.ticketType == 'BUG')return '<b style="color:lightgreen;">BUG</b>';
                          else return '<b style="color:lightgreen;">TASK</b>';
                        }

                          itemsArr.push(
                              {
                                  xtype: 'panel',
                                  style: 'margin:0px 0px 10px;border-radius:10px',
                                  bodyStyle: 'background-color:rgba(255,255,255,0.1);',
                                  minHeight: '20vh',
                                  width: '100%',
                                  itemId: idx,
                                  layout: {
                                      type: 'vbox',
                                      align: 'stretch'
                                  },
                                  header: {
                                    titlePosition: 0,
                                    titleAlign: 'center',
                                    title:`#${idx+1} Task`,
                                    style:function () {

                                    },
                                    minHeight: '1em',
                                    height: 40,
                                    items: [{
                                            xtype: 'button',
                                            iconCls: 'md-icon-mode-edit',
                                            align: 'right',
                                            handler:function () {me.goToTask(idx);}
                                          }],
                                  },
                                  items: [{
                                      html: `
                                        <div style="">
                                        <p style="text-align:center;font-size:20px;padding:0px 20px 0px;white-space: nowrap;text-overflow: ellipsis;">
                                          <b>${item.summary}</b>
                                        </p>
                                        </div>
                                        <p style="text-align:justify;padding:0px 10px 0px;">
                                          <b>Description: </b>${item.description}
                                        </p>
                                        <p style="text-align:justify;padding:0px 10px 0px;">
                                          <b>Posted by: </b><b style="color:lightgreen;">${item.creatorUsername}</b>
                                        </p>
                                        <p style="text-align:justify;padding:0px 10px 0px;">
                                          <b>Ticket type: </b>${getType()}
                                        </p>
                                        <p style="text-align:justify;padding:0px 10px 0px;">
                                          <b>Story Points: </b><b style="color:lightgreen;">${item.points}</b>
                                        </p>
                                        <p style="text-align:justify;padding:0px 10px 0px;">
                                          <b>Status: </b>${getStatus()}
                                        </p>
      `,


                                      "issuerId": 0,
                                      "storyPoints": 0,
                                      "summary": "string",
                                      "type": "TASK"
                                  }, {
                                      xtype: 'button',
                                      text: (function () {
                                        if(item.assigneeUsername != '' && item.assigneeUsername != null && item.assigneeUsername != undefined && item.assigneeUsername == localStorage.getItem('token'))return 'APPLIED';
                                        else if(item.assigneeUsername != '' && item.assigneeUsername != null && item.assigneeUsername != undefined && item.assigneeUsername != localStorage.getItem('token'))return `TAKEN BY ${item.assigneeUsername}`;
                                        else return 'APPLY FOR TASK';
                                      })(),
                                      //icon: 'x-fa fa-compose',
                                      handler: function () {
                                        if(item.assigneeUsername != '' && item.assigneeUsername != null && item.assigneeUsername != undefined && item.assigneeUsername == localStorage.getItem('token')){
                                          Ext.Msg.alert('Task status', 'You have already applied for this task!');
                                        }
                                        else if(item.assigneeUsername != '' && item.assigneeUsername != null && item.assigneeUsername != undefined && item.assigneeUsername != localStorage.getItem('token')){
                                          Ext.Msg.alert('Task status', `This task is already taken by ${item.assigneeUsername}.`);
                                        }
                                        else {
                                          Ext.Msg.confirm('Apply confirmation','Are you sure you want to apply for this task?', function (btnText) {
                                                if(btnText == 'yes')
                                                {
                                                  Ext.Ajax.request({
                                                    defaultHeaders: {
                                                      "Content-Type" : "application/json",
                                                      "Token" : localStorage.getItem('dbToken'),
                                                    },
                                                    url: 'http://localhost:8080/tickets/user',
                                                    method: 'PUT',
                                                    params: Ext.encode({
                                                      ticket_id: Number(item.id),
                                                      username: localStorage.getItem('token')
                                                    }),
                                                    success: function () {
                                                      Ext.Msg.alert('Task applied', 'You have successfuly applied for this task!');
                                                      me.loadInfo();
                                                    },
                                                    failure: function (res) {
                                                          Ext.Msg.alert("Error","Server-side failure with status code " + res.status);
                                                    }
                                                  });
                                                }
                                          });
                                        }
                                      },
                                      style: 'padding-bottom:10px;'
                                  }],
                              });


                      });
                      this.getView().lookup('myItems').setItems(itemsArr);
                  },
                  failure: function (res, opt) {
                      Ext.Msg.alert("Error","Server-side failure with status code " + res.status);
                  }
              });
            },
            failure: function (response, options) {
                Ext.Msg.alert("Error","Server-side failure with status code " + response.status);
            }
        });




    }


});