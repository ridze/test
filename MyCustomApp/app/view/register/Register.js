//za validaciju
var form = new Object;

Ext.define('MyApp.view.register.Register', {
    extend: 'Ext.form.Panel',
    xtype: 'register',
    
    reference: 'register',
    
    controller: 'register',
    model: 'register',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    style:'height:100vh',
    autoScroll: true,
    scrollable: true,
    header:{
    width:'100%',
    padding:0,   
    items:[
    {
        xtype:'container',
        width:'100%',
        height:65,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        margin:0,
        items:[{
            xtype:'container',
            flex:1,
            height:'100%',
            //style:'background-color:#2196f3',
            buttonAlign:'center',
            items:[{
                xtype:'button',
                text:'Login',
                cls: 'login-shadow',
                //style:'color:white',
                width:'100%',
                height:65,
                handler:'toLogIn'
            }]
        },{
            xtype:'container',
            flex:1,
            height:'100%',
            items:[{
                xtype:'button',
                text:'Register',
                cls: 'register-shadow',
                //style:'background-color:#fafafa; border-radius:0px;',
                width:'100%',
                height:65,                
            }]
        }]
    }]
    },
    items: [{
        xtype: 'textfield',
        cls: 'register-icon-user',
        label: 'Username',
        name:'username',
        reference:'username',
        required: true,
        enabled:true,

    }, {
        xtype: 'textfield',
        label: 'First Name',
        name:'firstName',
        required: true,

    }, {
        xtype: 'textfield',
        label: 'Last Name',
        name:'lastName',
        required: true,

    }, {
        xtype: 'emailfield',
        cls: 'register-icon-email',
        label: 'Email',
        name:'email',
        required: true,
     
    }, {
        xtype: 'passwordfield',
        cls: 'register-icon-pass',
        label: 'Password',
        name:'password',
        required: true,
        enabled:true,
        reference:'pass',
       
    }, {
        xtype: 'passwordfield',
        label: 'Confirm Password',
        name:'repeatedPassword',
        required: true,
        reference:'repPass',
        
    }],
   
    buttonAlign : 'center', 
      
    buttons:[{
        text: 'Submit',
        reference: 'registerBtn',
        width:'50%',
        handler:'register',
    }]
    


});