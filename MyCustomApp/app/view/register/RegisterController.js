Ext.define('MyApp.view.register.RegisterController', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.register',
    controller:'logIn',

    toLogIn: function(){
        this.redirectTo('Login');
    },

    register: function(){
        if ((this.getView().validate()) && (this.getView().isValid())) {
            
            if (this.lookupReference('pass').getValue()===this.lookupReference('repPass').getValue()) { 

                var logInData = this.getView().getValues();
            
                Ext.Ajax.request({
                    defaultHeaders:{
                        'Content-Type': 'application/json'
                    },
                    scope:this,
                    url: 'http://localhost:8080/users/register',
                    params: Ext.encode(this.getView().getValues()),

                    success: function(response, opts) {
                        Ext.Msg.alert('Register', 'You have registered successfully.');
                        this.redirectTo('Login');
                        

                    },

                    failure: function(response, opts) {
                        console.log('server-side failure with status code ' + response.status);

                    }
                });
            }
            else{
                Ext.Msg.alert('Passwords do not match.');
            }
        }
        else {
            Ext.Msg.alert('Please, fill all fields.');  
        }      
    }









});
