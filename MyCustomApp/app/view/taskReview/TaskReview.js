Ext.define('MyApp.view.taskReview.TaskReview', {
    extend: 'Ext.form.Panel',
    xtype: 'taskReview',
    reference: 'taskReview',

    controller: 'taskReview',
    viewModel: 'taskReview',
    title: 'Task review',
    header: {
      titlePosition: 0,
      titleAlign: 'left',
      items: [{
              xtype: 'button',
              iconCls: 'md-icon-delete',
              align: 'right',
              handler:'deleteTask'
            }],
    },
    layout: {
      type: 'vbox'
    },
    listeners: {
        show: 'getTask',
    },
    scrollable:true,
    height:'calc(100vh - 100px)',
    items:[{
        xtype: 'textfield',
        label: 'Project name',
        name:'projectName',
        reference:'projectName',
        textAlign:'center',
        style: "padding: 0px 10px 0px;",
      },{
        layout: {
          type:'hbox'
        },
        items: [{
          flex:1,
          xtype: 'selectfield',
          style: "padding: 0px 10px 0px;",
          textAlign:'center',
          reference: 'selectType',
          label: 'Ticket type',
          options: [{
              text: 'NEW FEATURE',
              value: 'NEW_FEATURE'
          }, {
              text: 'IMPROVEMENT',
              value: 'IMPROVEMENT'
          }, {
              text: 'BUG',
              value: 'BUG'
          }, {
              text: 'TASK',
              value: 'TASK'
          }]
        },{
          flex: 1,
          xtype: 'selectfield',
          style: "padding: 0px 10px 0px;",
          textAlign:'center',
          reference: 'selectStatus',
          label: 'Status',
          options: [{
              text: 'ACTIVE',
              value: 'TO_DO'
          }, {
              text: 'IN PROGRESS',
              value: 'IN_PROGRESS'
          }, {
              text: 'RESOLVED',
              value: 'RESOLVED'
          }, {
              text: 'DISABLED',
              value: 'DISABLED'
          }]

        }]

      },{
        xtype: 'selectfield',
        style: "padding: 0px 10px 0px;",
        textAlign:'center',
        reference: 'selectUser',
        label: 'Change user for the ticket',
      },{
        xtype: 'textareafield',
        label: 'Project description',
        name:'projectDesc',
        reference:'projectDesc',
        textAlign:'justify',
        height:'calc(100vh - 500px)',
        minHeight:200,
        style: "padding: 0px 10px 0px;",
      },{
        layout: {
          type: 'hbox'
        },
        items: [{
          reference: 'firstDate',
          height:90,
          flex:1,
          html:`<p style="text-align:center;">Start date:</br><b style="font-size:16px;">0000-00-00</b></p>`,
        },{
          reference: 'secondDate',
          height:90,
          flex:1,
          html:`<p style="text-align:center;">End date:</br><b style="font-size:16px;">0000-00-00</b></p>`,
        }]
      },{
        xtype:'panel',
        buttons:[{
          text:'SAVE',
          handler: 'saveTask'
        },{
          text:'CANCEL',
          handler: function(){
              Ext.Msg.confirm('Cancel','Are you sure you want to cancel updating ticket information? All unsaved data will be lost!', function (btnText) {
                if(btnText == 'yes')window.history.back();
              });
          }
        }],
      }],

});