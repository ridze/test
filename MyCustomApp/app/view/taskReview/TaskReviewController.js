Ext.define('MyApp.view.taskReview.TaskReviewController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.taskReview',
    getTicketId: function(){return 0;},
    deleteTask: function () {
      var currUrl = window.location.hash.slice(1);
      var tId = currUrl.split('/')[3];console.log(tId);
      var par = this;
      Ext.Msg.confirm('Delete task','Are you sure you want to delete this task?', function (btnText) {
            if(btnText == 'yes'){
              Ext.Ajax.request({
                method: "DELETE",
                defaultHeaders: {
                  "Content-Type" : "application/json",
                  "Token" : localStorage.getItem('dbToken'),
                },
                url: `http://localhost:8080/tickets/${par.getTicketId()}`,
                scope:this,
                success: function () {
                  Ext.Msg.alert("Delete","You have successfuly deleted this task!", function() {
              		window.history.back();
                });
                },
                failure: function (res,opts) {
                  Ext.Msg.alert("Error","Server-side failure with status code " + res.status);
                }
              });
            }
			});
    },
    saveTask: function () {
      var currUrl = window.location.hash.slice(1);
      var tId = currUrl.split('/')[3];console.log(tId);
      var par = this;
      console.log(par.getTicketId());
      Ext.Msg.confirm('Save changes','Are you sure you want to save your changes?', function (btnText) {
            if(btnText == 'yes'){
              console.log(par.getTicketId());
              Ext.Ajax.request({
                method: "PUT",
                defaultHeaders: {
                  "Content-Type" : "application/json",
                  "Token" : localStorage.getItem('dbToken'),
                },
                url: "http://localhost:8080/tickets",
                scope:this,
                params: Ext.encode({
                  description: par.getView().lookup('projectDesc').getValue(),
                  id: par.getTicketId(),
                  points: 0,
                  status: par.getView().lookup('selectStatus').getValue(),
                  summary: par.getView().lookup('projectName').getValue(),
                  ticketType: par.getView().lookup('selectType').getValue(),
                  username: par.getView().lookup('selectUser').getValue()
                }),
                success: function (res,opts) {
                  Ext.Msg.alert("Update","You have successfuly updated your task info!", function() {
        		window.history.back();
			});
                },
                failure: function (res,opts) {
                  Ext.Msg.alert("Error","Server-side failure with status code " + res.status);
                }
              });
            }
          });
    },
    getTask:function () {
      var parThis = this;
      console.log("WORKS");
        var currUrl = window.location.hash.slice(1);
        var pId = currUrl.split('/')[1];console.log(pId);
        var tId = currUrl.split('/')[3];console.log(tId);
        var storedNames = JSON.parse(localStorage.getItem("projectNames"));
        var currentProjectName = storedNames[pId];
        var myUrl = "http://localhost:8080/projects/"+currentProjectName+"/tickets"
        //Ajax request match ID i show
        Ext.Ajax.request({
          method: "GET",
          defaultHeaders: {
            "Content-Type" : "application/json",
            "Token" : localStorage.getItem('dbToken'),
          },
          url: myUrl,
          scope:this,

          success: function (response, options) {
            Ext.Ajax.request({
              method: "GET",
              defaultHeaders: {
                "Content-Type" : "application/json",
                "Token" : localStorage.getItem('dbToken'),
              },
              url: 'http://localhost:8080/users',
              scope:this,
              success: function (resUsr,opts) {
                var usersArr = [{text:'None',value: null}];
                var usersRes = Ext.JSON.decode(resUsr.responseText);
                usersRes.forEach(function (item,idx) {
                  usersArr.push({text:item.username,value:item.username});
                });
                this.getView().lookup('selectUser').setOptions(usersArr);
                var res = Ext.JSON.decode(response.responseText)[tId];
                this.getView().lookup('selectUser').setValue(res.assigneeUsername);
                parThis.getTicketId = function () {
            		  let newId = res.id;
                  return newId;
                };
                this.getView().lookup('projectName').setValue(res.summary);
                this.getView().lookup('projectDesc').setValue(res.description);
                this.getView().lookup('selectType').setValue(res.ticketType);
                this.getView().lookup('selectStatus').setValue(res.status);
                this.getView().lookup('firstDate').setHtml(`<p style="text-align:center;">Created date:</br><b style="font-size:16px;">${res.createdDate.substring(0,10)}</b></p>`);
                this.getView().lookup('secondDate').setHtml(`<p style="text-align:center;">Resolve date:</br><b style="font-size:16px;">${res.resolvedDate.substring(0,10)}</b></p>`);
              },
              failure: function (response, options) {
                  Ext.Msg.alert("Error","Server-side failure with status code " + response.status);
              }

            });

          },
          failure: function (response, options) {
              Ext.Msg.alert("Error","Server-side failure with status code " + response.status);
          }
        });
    }

});